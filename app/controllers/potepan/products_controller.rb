class Potepan::ProductsController < ApplicationController
  def show
    @product = Spree::Product.find(params[:id])
    @taxon = @product.taxons.first
    @related_products = @product.related_products.include_master.
      limit(MAX_DISPLAY_RELATED_PRODUCTS)
  end
end
