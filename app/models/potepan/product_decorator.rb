# frozen_string_literal: true

module Potepan::ProductDecorator
  def self.prepended(base)
    base.scope :include_master, -> {
      includes(master: [:default_price, :images])
    }
  end

  def related_products
    Spree::Product.in_taxons(taxons).
      distinct.where.not(id: id)
  end

  Spree::Product.prepend self
end
