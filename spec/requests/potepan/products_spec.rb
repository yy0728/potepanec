require 'rails_helper'

RSpec.describe "Potepan::Products", type: :request do
  let(:taxon) { create(:taxon) }
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

  describe 'GET #show' do
    before { get potepan_product_path(product.id) }

    it "200レスポンスを返す" do
      expect(response).to have_http_status(200)
    end

    it 'showテンプレートが表示される' do
      expect(response).to render_template :show
    end

    it '商品情報が表示される' do
      aggregate_failures do
        expect(response.body).to include product.name
        expect(response.body).to include product.display_price.to_s
        expect(response.body).to include product.description
      end
    end

    it '商品が持つカテゴリーページのパスがレスポンスに含まれる' do
      expect(response.body).to include potepan_category_path(taxon.id)
    end

    it "関連商品が4つ表示される(5つ以上は表示されない)" do
      aggregate_failures do
        0.upto(3).each do |index|
          expect(response.body).to include related_products[index].name
          expect(response.body).to include related_products[index].display_price.to_s
        end
        expect(response.body).not_to include related_products.last.name
      end
    end
  end
end
