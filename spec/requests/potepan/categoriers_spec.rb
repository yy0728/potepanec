require 'rails_helper'

RSpec.describe "Potepan::Categoriers", type: :request do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, taxonomy: taxonomy) }
  let!(:product) { create(:product, taxons: [taxon]) }

  describe 'GET #show' do
    before { get potepan_category_path(taxon.id) }

    it "200レスポンスを返す" do
      expect(response).to have_http_status(200)
    end

    it 'showテンプレートが表示される' do
      expect(response).to render_template :show
    end

    it '商品情報及びカテゴリー名が表示される' do
      aggregate_failures do
        expect(response.body).to include taxonomy.name
        expect(response.body).to include taxon.name
        expect(response.body).to include product.name
        expect(response.body).to include product.display_price.to_s
      end
    end
  end
end
