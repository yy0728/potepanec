require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "full_titleメソッド" do
    it '引数が空白の時にサイト名のみ表示される' do
      expect(full_title("")).to eq("#{SITE_TITLE}")
    end

    it '引数がnilの時にサイト名のみ表示される' do
      expect(full_title(nil)).to eq("#{SITE_TITLE}")
    end

    it '引数にページタイトルがある時にサイト名とページタイトルが表示される' do
      expect(full_title("page title")).to eq("page title - #{SITE_TITLE}")
    end
  end
end
