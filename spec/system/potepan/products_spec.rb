require 'rails_helper'

RSpec.describe 'Products', type: :system do
  describe '商品概要に関するテスト' do
    let(:taxon) { create(:taxon) }
    let(:product) { create(:product, taxons: [taxon]) }
    let(:product_without_taxon) { create(:product, taxons: []) }

    context 'カテゴリーを持つ商品の場合' do
      before do
        visit potepan_product_path(product.id)
      end

      it '商品情報が表示される' do
        aggregate_failures do
          expect(page).to have_content product.name
          expect(page).to have_content product.display_price
          expect(page).to have_content product.description
        end
      end

      it '一覧ページへ戻るをクリック時にカテゴリーページに遷移する' do
        click_link '一覧ページへ戻る'
        expect(page).to have_current_path potepan_category_path(taxon.id)
      end

      it 'ページタイトルに商品名が表示される' do
        expect(page).to have_title(full_title(product.name))
      end
    end

    context 'カテゴリーを持たない商品の場合' do
      before do
        visit potepan_product_path(product_without_taxon.id)
      end

      it "カテゴリーページへのリンクを表示しない" do
        expect(page).not_to have_content '一覧ページへ戻る'
      end
    end
  end

  describe '関連商品に関するテスト' do
    let!(:taxon) { create(:taxon) }
    let!(:other_taxon) { create(:taxon) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:unrelated_products) { create(:product, taxons: [other_taxon]) }
    let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

    before do
      visit potepan_product_path(product.id)
    end

    it '関連商品する商品のみが4つ表示される(5つ以上は表示されない)' do
      aggregate_failures do
        0.upto(3).each do |index|
          expect(page).to have_content related_products[index].name
          expect(page).to have_content related_products[index].price
        end
        expect(page).not_to have_content unrelated_products.name
        expect(page).not_to have_content related_products.last.name
      end
    end

    it '関連商品をクリックした時にその商品の商品詳細ページに遷移する' do
      click_link related_products[0].name
      expect(page).to have_current_path potepan_product_path(related_products[0].id)
    end
  end

  describe 'ヘッダーに関するテスト' do
    let(:product) { create(:product) }

    before do
      visit potepan_product_path(product.id)
    end

    it 'LightSectionのHomeボタン押下時にトップページに遷移する' do
      within('.lightSection') do
        click_link "Home"
      end
      expect(page).to have_current_path potepan_path
    end

    it 'HeaderのHomeボタン押下時にトップページに遷移する' do
      within('.header') do
        click_link "Home"
      end
      expect(page).to have_current_path potepan_path
    end
  end
end
