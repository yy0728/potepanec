require 'rails_helper'

RSpec.describe 'Categories', type: :system do
  let!(:taxonomy) { create(:taxonomy) }
  let!(:taxon) { create(:taxon, taxonomy: taxonomy, name: 'category') }
  let!(:other_taxon) { create(:taxon, taxonomy: taxonomy, name: 'other_category') }
  let!(:product) { create(:product, taxons: [taxon], name: 'product name') }
  let!(:other_product) { create(:product, taxons: [other_taxon], name: 'other product name') }

  before do
    visit potepan_category_path(taxon.id)
  end

  it '全てのカテゴリーが表示される' do
    aggregate_failures do
      expect(page).to have_content taxonomy.name
      expect(page).to have_content taxon.name
      expect(page).to have_content other_taxon.name
    end
  end

  it '指定したカテゴリーの商品のみが表示される' do
    aggregate_failures do
      expect(page).to have_content product.name
      expect(page).not_to have_content other_product.name
      expect(page).to have_content product.price
    end
  end

  it 'カテゴリー名をクリック時にカテゴリーページに遷移する' do
    within('.sideBar') do
      click_link other_taxon.name
    end
    expect(page).to have_current_path potepan_category_path(other_taxon.id)
  end

  it '商品名をクリック時に商品詳細ページに遷移する' do
    within('.productBox') do
      click_link product.name
    end
    expect(page).to have_current_path potepan_product_path(product.id)
  end

  it 'ページタイトルにカテゴリー名が表示される' do
    expect(page).to have_title(full_title(taxon.name))
  end
end
