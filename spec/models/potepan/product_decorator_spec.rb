require 'rails_helper'

RSpec.describe Potepan::ProductDecorator do
  let(:taxon) { create(:taxon) }
  let(:taxon2) { create(:taxon) }
  # 重複しないことをテストするため複数のtaxonを持たせる
  let(:product) { create(:product, taxons: [taxon, taxon2]) }
  let(:related_products) { create_list(:product, 5, taxons: [taxon, taxon2]) }
  let(:other_product) { create(:product, taxons: []) }

  describe "#related_products" do
    it '対象となる商品と同じtaxonの商品は全て関連商品になる' do
      expect(product.related_products).to match related_products
    end

    it '対象となる商品は関連商品に含まれない' do
      expect(product.related_products).not_to include product
    end

    it '対象となる商品と同じtaxonを持たない商品は関連商品に含まれない' do
      expect(product.related_products).not_to include other_product
    end

    it '関連商品の配列に重複がない' do
      expect(product.related_products).to eq product.related_products.uniq
    end
  end
end
