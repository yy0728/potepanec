# 定数を定義する

# サイトタイトル
SITE_TITLE = 'BIGBAG Store'.freeze

# 関連商品の最大表示数
MAX_DISPLAY_RELATED_PRODUCTS = 4
